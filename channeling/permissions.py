
from channeling import message_formatting as mf

def administrator(f):
    async def wrapper(*args, **kw):
        if not args[0].author.top_role.permissions.administrator:
            return await args[0].channel.send(mf.error("You need administrator permissions to use this command. name:"+str(args[0].author.top_role.name)))
        else:
            return await f(*args, **kw)
    return wrapper

