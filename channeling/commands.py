"""
All available commands aggregated in one scope.
To fetch a command use: get_command(cmd)
"""

from channeling import ChannelingException
from channeling import batteries
from channeling import message_formatting as mf
from channeling import permissions
from channeling import tools
from channeling.batteries import cmd_channel
from channeling.batteries import cmd_grab
from channeling.batteries import cmd_info
from channeling.batteries import cmd_power
from channeling.batteries import cmd_set
from channeling.batteries import cmd_stats
from channeling.cmd_joke import cmd_joke
from channeling.config import ChannelingConfig
from discord import utils as du
import channeling

import json
import os
import shlex
import sys
import time

conf = ChannelingConfig()
batteries.__db_init__()

@conf.debug
async def cmd_version(message):
    """Display version number and info.
    Usage: `version`
    """
    return await message.channel.send(mf.info(channeling.__version__))

@conf.debug
async def cmd_help(message):
    """Display help.
    Usage: `help [COMMAND]`
    """
    line = shlex.split(message.content)
    help_string = ""
    if len(line) == 3:
        try:
            cmd = get_command(line[2])
        except:
            return await message.channel.send(mf.error(
                "Can't display help for unknown command: {}".format(line[2])))
        help_string = os.linesep.join([s for s in cmd.__doc__.splitlines() if s.strip()])
        return await message.channel.send(mf.info(help_string))
    help_string = "Channeling commands:\n\n"
    all_commands = get_all_commands()
    for command_name in all_commands:
        help_string += "{} - {}\n".format(
                command_name,
                all_commands[command_name].__doc__.split("\n")[0]
                )
    help_string += "\nUse `help COMMAND` to display more info on specific command."
    return await message.channel.send(mf.info(help_string))

@conf.debug
async def cmd_bugreport(message):
    """Report a bug.
    Usage: `bugreport Text information about the bug you encountered.`
    """
    conf.logger.info(json.dumps({
        'timestamp': message.created_at.timestamp(),
        'type': 'bugreport',
        'author_name': message.author.display_name,
        'author_id': message.author.id,
        'channel_name': message.channel.name,
        'channel_id': message.channel.id,
        'guild_name': message.guild.name,
        'guild_id': message.guild.id,
        'content': message.content
        }))
    return await message.channel.send(mf.info("Your help in improving channeling bot is very appreciated!"))

@conf.debug
async def cmd_rolemembers(message, client):
    """List all memebers with a specific role.
    Usage: `rolemembers [ROLE]`
    """
    line = shlex.split(message.content)
    if len(line) != 3:
        return await message.channel.send(mf.error("Bad arguments. Use `help rolemembers` for more info."))
    role_tag = line[2]
    role_id = tools.role_id_from_role_tag(role_tag)
    if not role_id:
        return await message.channel.send(mf.error("Role tag not recognized."))
    guild = None
    for g in client.guilds:
        if g.id == message.guild.id:
            guild = g
            break
    role = None
    for r in guild.roles:
        if str(r.id) == role_id:
            role = r
            break
    members_string = "guild:{}\nRole {} has {} users:".format(guild.name, role.name, str(len(role_members)))
    for member in role.members:
        members_string += "\n'{}#{}', {}".format(member.name, member.discriminator, str(member.id))
    return await message.channel.send(members_string)
    
@conf.debug
@permissions.administrator
async def cmd_kickrole(message, client):
    """<ADMIN> Kick all memebers with a specific role.
    Usage: `kickrole [ROLE] [REASON]`
    """
    line = shlex.split(message.content)
    if len(line) != 4:
        return await message.channel.send(mf.error("Bad arguments. Use `help rolemembers` for more info."))
    role_tag = line[2]
    reason = line[3]
    role_id = tools.role_id_from_role_tag(role_tag)
    if not role_id:
        return await message.channel.send(mf.error("Role tag not recognized."))
    guild = None
    for g in client.guilds:
        if g.id == message.guild.id:
            guild = g
            break
    role = None
    for r in guild.roles:
        if str(r.id) == role_id:
            role = r
            break
    count = len(role.members)
    for member in role.members:
        await member.kick(reason=reason)
    return await message.channel.send(mf.info("I kicked {} users with {} role.".format(count, role_tag)))
 
def get_command(cmd):
    return getattr(sys.modules[__name__], 'cmd_'+cmd)

def get_all_commands():
    all_commands = {}
    for x in dir(sys.modules[__name__]):
        if x.startswith("cmd_"):
            all_commands[x[4:]] = get_command(x[4:])
    return all_commands

async def parse(message, client):
    guild = batteries.Guild(message.guild.id)
    await general_processing(message)
    line = []
    try:
        line = shlex.split(message.content)
    except ValueError as e:
        line = message.content.split()
    if not len(line):
        return
    if line[0] != guild.prefix:
        return None
    if len(line) == 1:
        return await cmd_help(message)
    cmd = line[1]
    if cmd == "rolemembers":
        return await cmd_rolemembers(message, client)
    if cmd == "kickrole":
        return await cmd_kickrole(message, client)
    cmd_func = None
    try:
        cmd_func = get_command(cmd)
        assert cmd_func != None
    except AttributeError as e:
        conf.logger.debug(json.dumps({
            'timestamp': str(time.time()),
            'type': 'unknown_cmd',
            'content': message.content,
            'error': str(e)
            }))
        return await message.channel.send(
            mf.error("Unknown command '{}'. Use 'help' for list of available commands.".format(cmd)))
    try:
        return await cmd_func(message)
    except AttributeError as e:
        conf.logger.debug(json.dumps({
            'timestamp': str(time.time()),
            'type': 'error',
            'content': message.content,
            'error': str(e)
            }))
        return await message.channel.send(
            mf.error("I made an oopsie. 😅"))

async def general_processing(message):
    if not message.author.bot:
        return await batteries.User(message.author.id, message.guild.id).talk(message)

