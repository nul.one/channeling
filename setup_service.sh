#!/bin/bash

set -e

script_dir=$(cd `dirname "$0"`; pwd; cd - 2>&1 >> /dev/null)

mkdir -p /etc/systemd/system
cat $script_dir/systemd.service > /etc/systemd/system/channeling.service
mkdir -p /etc/channeling
mkdir -p /var/lib/channeling
mkdir -p /var/log/channeling

